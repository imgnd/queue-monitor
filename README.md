# Queue Monitor

[![Latest Stable Version](https://img.shields.io/packagist/v/imgnd/queue-monitor)](https://packagist.org/packages/imgnd/queue-monitor)
[![Total Downloads](https://img.shields.io/packagist/dt/imgnd/queue-monitor)](https://packagist.org/packages/imgnd/queue-monitor)
[![License](https://img.shields.io/packagist/l/imgnd/queue-monitor)](https://packagist.org/packages/imgnd/queue-monitor)
[![php](https://img.shields.io/badge/php->8.1-green.svg)](https://packagist.org/packages/imgnd/queue-monitor)

Laravel Queued Jobs Monitor for Filament ([filament/filament](https://filamentphp.com))

## Installation

Install the package via composer:

```bash
composer require imgnd/queue-monitor
```

## Configuration

The global plugin config can be published using the command below:

```bash
php artisan vendor:publish --tag="queue-monitor-config"
```

## Use in Filament Panels

You can register the plugin to your Panel configuration. This will register the plugin's resources.

For example in your `app/Providers/Filament/AdminPanelProvider.php` file:

```php
<?php


use Imagined\QueueMonitor\QueueMonitorPlugin;

...

public function panel(Panel $panel): Panel
{
    return $panel
        ->plugins([
            QueueMonitorPlugin::make()
        ]);
}
```
