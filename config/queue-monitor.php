<?php

return [
    'shouldRegisterNavigation' => true,
    'getNavigationGroup' => null,
    'getNavigationSort' => 9999,
    'getNavigationIcon' => 'heroicon-o-queue-list'
];
