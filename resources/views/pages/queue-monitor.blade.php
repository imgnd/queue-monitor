<x-filament-panels::page>
    @livewire('queue-monitor::list-queued-jobs')
    @livewire('queue-monitor::list-failed-jobs')
</x-filament-panels::page>
