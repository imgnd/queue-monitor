<?php

namespace Imagined\QueueMonitor\Filament\Pages;

use Filament\Pages\Page;
use Filament\Actions\Action;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Facades\Artisan;
use Imagined\QueueMonitor\Models\QueuedJob;
use Imagined\QueueMonitor\Models\FailedJob;

class QueueMonitor extends Page
{
    protected static string $view = 'queue-monitor::pages.queue-monitor';

    public static function shouldRegisterNavigation(): bool
    {
        return config('queue-monitor.shouldRegisterNavigation');
    }

    public static function getNavigationGroup(): ?string
    {
        return config('queue-monitor.getNavigationGroup');
    }

    public static function getNavigationSort(): ?int
    {
        return config('queue-monitor.getNavigationSort');
    }

    public static function getNavigationIcon(): string|Htmlable|null
    {
        return config('queue-monitor.getNavigationIcon');
    }

    public static function getNavigationLabel(): string
    {
        return __('Queue Monitor');
    }

    protected function getHeaderActions(): array
    {
        return [
            Action::make('queue')
                ->label(__('Work default queue once'))
                ->icon('heroicon-o-cpu-chip')
                ->color('info')
                ->action(fn() => $this->workQueue()),
            Action::make('failed')
                ->label(__('Retry all failed jobs'))
                ->icon('heroicon-o-arrow-path-rounded-square')
                ->color('warning')
                ->requiresConfirmation()
                ->action(fn() => $this->retryFailed())
        ];
    }

    public static function getNavigationBadge(): ?string
    {
        $failed = FailedJob::count();
        $queued = QueuedJob::count();
        if ($failed > 0) {
            return $failed;
        }
        if ($queued > 0) {
            return $queued;
        }
        return null;
    }

    public static function getNavigationBadgeColor(): string|array|null
    {
        $failed = FailedJob::count();
        $queued = QueuedJob::count();
        if ($failed > 0) {
            return 'danger';
        }
        if ($queued > 0) {
            return 'info';
        }
        return null;
    }

    private function workQueue()
    {
        Artisan::call('queue:work -q -n --stop-when-empty');
        $this->dispatch('refresh');
    }

    private function retryFailed()
    {
        Artisan::call('queue:retry all -q -n');
        $this->dispatch('refresh');
    }
}
