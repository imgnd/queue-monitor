<?php

namespace Imagined\QueueMonitor;

use Filament\Contracts\Plugin;
use Filament\Panel;

class QueueMonitorPlugin implements Plugin
{
    public function getId(): string
    {
        return QueueMonitorServiceProvider::$name;
    }

    public function register(Panel $panel): void
    {
        $panel->pages([
            \Imagined\QueueMonitor\Filament\Pages\QueueMonitor::class,
        ]);
    }

    public function boot(Panel $panel): void
    {
        //
    }

    public static function make(): static
    {
        return app(static::class);
    }

    public static function get(): static
    {
        /** @var static $plugin */
        $plugin = filament(app(static::class))->getId();
        return $plugin;
    }
}