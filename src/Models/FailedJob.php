<?php

namespace Imagined\QueueMonitor\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FailedJob extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'failed_jobs';

    protected $guarded = [];

    protected $casts = [
        'payload' => 'array',
        'failed_at' => 'datetime',
    ];
}