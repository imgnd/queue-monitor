<?php

namespace Imagined\QueueMonitor\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class QueuedJob extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'jobs';

    protected $guarded = [];

    protected $casts = [
        'payload' => 'array',
        'reserved_at' => 'timestamp',
        'available_at' => 'timestamp',
        'created_at' => 'timestamp',
    ];
}