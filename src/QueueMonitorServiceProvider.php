<?php

namespace Imagined\QueueMonitor;

use Imagined\QueueMonitor\Livewire\ListFailedJobs;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Spatie\LaravelPackageTools\Commands\InstallCommand;
use Livewire\Livewire;
use Imagined\QueueMonitor\Livewire\ListQueuedJobs;

class QueueMonitorServiceProvider extends PackageServiceProvider
{
    public static string $name = 'queue-monitor';

    public function configurePackage(Package $package): void
    {
        $package->name(static::$name)
            ->hasInstallCommand(function (InstallCommand $command) {
                $command->publishConfigFile();
            });

        $configFileName = $package->shortName();

        if (file_exists($package->basePath("/../config/{$configFileName}.php"))) {
            $package->hasConfigFile();
        }
        if (file_exists($package->basePath('/../resources/views'))) {
            $package->hasViews(static::$name);
        }
    }

    public function packageBooted()
    {
        Livewire::component('queue-monitor::list-queued-jobs', ListQueuedJobs::class);
        Livewire::component('queue-monitor::list-failed-jobs', ListFailedJobs::class);
    }
}