<?php

namespace Imagined\QueueMonitor\Livewire;

use Illuminate\Support\Facades\Artisan;
use Livewire\Attributes\On;
use Livewire\Component;
use Filament\Forms\Contracts\HasForms;
use Filament\Tables\Contracts\HasTable;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Forms;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Support\Enums\ActionSize;
use Filament\Support\Enums\MaxWidth;
use Imagined\QueueMonitor\Models\QueuedJob;

class ListQueuedJobs extends Component implements HasForms, HasTable
{
    use InteractsWithForms, InteractsWithTable;

    public function table(Table $table): Table
    {
        return $table
            ->query(QueuedJob::query())
            ->heading(__('Queued Jobs'))
            ->columns([
                Tables\Columns\TextColumn::make('payload.uuid')
                    ->label(__('Id')),
                Tables\Columns\TextColumn::make('queue'),
                Tables\Columns\TextColumn::make('payload.displayName')
                    ->label(__('Job class')),
                Tables\Columns\TextColumn::make('attempts')
                    ->toggleable(true, true),
                Tables\Columns\TextColumn::make('reserved_at')
                    ->dateTime('Y-m-d H:i:s')
                    ->sortable()
                    ->toggleable(true, true),
                Tables\Columns\TextColumn::make('available_at')
                    ->dateTime('Y-m-d H:i:s')
                    ->sortable()
                    ->toggleable(true, false),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('Y-m-d H:i:s')
                    ->sortable()
                    ->toggleable(true, true),
            ])
            ->defaultSort('created_at', 'desc')
            ->filters([])
            ->actions([
                Tables\Actions\Action::make('work')
                    ->label(__('Run job'))
                    ->icon('heroicon-o-cog-8-tooth')
                    ->color('info')
                    ->button()
                    ->size(ActionSize::ExtraSmall)
                    ->action(fn($record) => $this->workJob($record)),
                Tables\Actions\EditAction::make()->label('')
                    ->form($this->editQueuedJob())
                    ->modalHeading(fn($record) => $record->payload['uuid'])
                    ->modalWidth(MaxWidth::SevenExtraLarge),
                Tables\Actions\DeleteAction::make()->label(''),
            ]);
    }

    private function editQueuedJob()
    {
        return [
            Forms\Components\Split::make([
                Forms\Components\Grid::make(1)->schema([
                    Forms\Components\KeyValue::make('payload')
                        ->label(__('Payload'))
                        ->disabled(),
                    Forms\Components\KeyValue::make('payload.data')
                        ->label(__('Data'))
                        ->disabled()
                ])->columnSpanFull(),
                Forms\Components\Grid::make(1)->schema([
                    Forms\Components\TextInput::make('queue')
                        ->prefixIcon('heroicon-o-queue-list')
                        ->required(),
                    Forms\Components\TextInput::make('attempts')
                        ->prefixIcon('heroicon-o-arrow-path-rounded-square'),
                    Forms\Components\DateTimePicker::make('reserved_at')
                        ->prefixIcon('heroicon-o-calendar-days')
                        ->format('U'),
                    Forms\Components\DateTimePicker::make('available_at')
                        ->prefixIcon('heroicon-o-calendar-days')
                        ->default(date('Y-m-d H:i:s'))
                        ->format('U')
                        ->required(),
                    Forms\Components\DateTimePicker::make('created_at')
                        ->prefixIcon('heroicon-o-calendar-days')
                        ->default(date('Y-m-d H:i:s'))
                        ->format('U')
                        ->required()
                        ->disabled(),
                ])->grow(false)
            ])->columnSpanFull(),
        ];
    }

    private function workJob($record): void
    {
        $record->queue = $record->payload['uuid'];
        $record->save();
        Artisan::call(sprintf('queue:work -q -n --queue=%s --once', $record->payload['uuid']));
        $this->dispatch('refresh');
    }

    #[On('refresh')]
    public function render()
    {
        return view('queue-monitor::livewire.list-jobs');
    }
}
