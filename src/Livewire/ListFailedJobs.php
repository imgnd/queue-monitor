<?php

namespace Imagined\QueueMonitor\Livewire;

use Illuminate\Support\Facades\Artisan;
use Livewire\Attributes\On;
use Livewire\Component;
use Filament\Forms\Contracts\HasForms;
use Filament\Tables\Contracts\HasTable;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Forms;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Support\Enums\ActionSize;
use Filament\Support\Enums\MaxWidth;
use Imagined\QueueMonitor\Models\FailedJob;

class ListFailedJobs extends Component implements HasForms, HasTable
{
    use InteractsWithForms, InteractsWithTable;

    public function table(Table $table): Table
    {
        return $table
            ->query(FailedJob::query())
            ->heading(__('Failed Jobs'))
            ->columns([
                Tables\Columns\TextColumn::make('uuid')
                    ->label(__('Id')),
                Tables\Columns\TextColumn::make('queue'),
                Tables\Columns\TextColumn::make('payload.displayName')
                    ->label(__('Job class')),
                Tables\Columns\TextColumn::make('failed_at')
                    ->dateTime('Y-m-d H:i:s')
                    ->sortable()
                    ->toggleable(true, false),
            ])
            ->defaultSort('failed_at', 'desc')
            ->filters([])
            ->actions([
                Tables\Actions\Action::make('retry')
                    ->label(__('Retry job'))
                    ->icon('heroicon-o-arrow-path-rounded-square')
                    ->color('warning')
                    ->button()
                    ->size(ActionSize::ExtraSmall)
                    ->action(fn($record) => $this->retryJob($record)),
                Tables\Actions\EditAction::make()->label('')
                    ->form($this->editFailedJob())
                    ->modalHeading(fn($record) => $record->uuid)
                    ->modalWidth(MaxWidth::SevenExtraLarge),
                Tables\Actions\DeleteAction::make()->label(''),
            ]);
    }

    private function editFailedJob()
    {
        return [
            Forms\Components\Split::make([
                Forms\Components\Grid::make(1)->schema([
                    Forms\Components\KeyValue::make('payload')
                        ->label(__('Payload'))
                        ->disabled(),
                    Forms\Components\KeyValue::make('payload.data')
                        ->label(__('Data'))
                        ->disabled()
                ])->columnSpanFull(),
                Forms\Components\Grid::make(1)->schema([
                    Forms\Components\TextInput::make('queue')
                        ->prefixIcon('heroicon-o-queue-list')
                        ->required(),
                    Forms\Components\DateTimePicker::make('failed_at')
                        ->prefixIcon('heroicon-o-calendar-days')
                        ->default(date('Y-m-d H:i:s'))
                        ->format('U')
                        ->disabled(),
                ])->grow(false)
            ])->columnSpanFull(),
            Forms\Components\Textarea::make('exception')
                ->rows(5)
        ];
    }

    private function retryJob($record): void
    {
        Artisan::call(sprintf('queue:retry %s -q -n', $record->payload['uuid']));
        $this->dispatch('refresh');
    }

    #[On('refresh')]
    public function render()
    {
        return view('queue-monitor::livewire.list-jobs');
    }
}
